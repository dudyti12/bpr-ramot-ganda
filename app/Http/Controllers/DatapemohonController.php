<?php

namespace App\Http\Controllers;

use App\Data_pemohon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;

class DatapemohonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
        return view('master');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pemohon.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data_pemohon= DB::table('data_pemohon')->insert([
            'nama_pemohon' => $request->nama_pemohon, 
            'tempat_lahir' => $request->tempat_lahir, 
            'umur' => $request->umur, 
            'alamat_tinggal' => $request->alamat_tinggal, 
            'no_ktp' => $request->no_ktp, 
            'no_npwp' => $request->no_npwp, 
            'nama_ibu' => $request->nama_ibu, 
            'pendidikan' => $request->pendidikan, 
            ]);
            return redirect('datapemohon')->with('status','Data Pemohon Berhasil Disimpan!');
      // dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Data_pemohon  $data_pemohon
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $data_pemohon = DB::table('data_pemohon')->get();
        return view('pemohon.data',compact('data_pemohon'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Data_pemohon  $data_pemohon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data_pemohon = DB::table('data_pemohon')->where('id_pemohon',$id)->first();
        return view('pemohon.edit', compact('data_pemohon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Data_pemohon  $data_pemohon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         DB::table('data_pemohon')
              ->where('id_pemohon', $id)
              ->update([
                'nama_pemohon' => $request->nama_pemohon, 
                'tempat_lahir' => $request->tempat_lahir, 
                'umur' => $request->umur, 
                'alamat_tinggal' => $request->alamat_tinggal, 
                'no_ktp' => $request->no_ktp, 
                'no_npwp' => $request->no_npwp, 
                'nama_ibu' => $request->nama_ibu, 
                'pendidikan' => $request->pendidikan 
                  
                  ]);
            return redirect('datapemohon')->with('status','Data Pemohon Berhasil Diupdate!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Data_pemohon  $data_pemohon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('data_pemohon')->where('id_pemohon',$id)->delete();
        return redirect('datapemohon')->with('status','Data Pemohon Berhasil Dihapus!');

    }
}

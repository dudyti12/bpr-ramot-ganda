<?php

namespace App\Http\Controllers;

use App\gambar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;
class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $this->validate($request, [
            'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'keterangan' => 'required',
            ]);
            
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
            
            $nama_file = time()."_".$file->getClientOriginalName();
            
                          // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);
            
            Gambar::create([
            'file' => $nama_file,
            'keterangan' => $request->keterangan,
            ]);
       return redirect('berkas')->with('status','Gambar Berhasil Disimpan!');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $gambar = DB::table('gambar')->get();
        return view('berkas.data',compact('gambar'));
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function edit(gambar $gambar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gambar $gambar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('gambar')->where('id', $id)->delete();
        return redirect('berkas')->with('status','Gambar Berhasil Dihapus!');
    }
}

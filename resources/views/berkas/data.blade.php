@extends('master')
@section('title','BPR GAMOT')
    
@section('breadcrumbs')
    
@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Upload  Gambar Bukti
            </div>
            <div class="pull-center">
             {{--   <a href="{{url('datapemohon/create')}}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i>Tambah
                </a>--}} 
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
               
               
                <div class="col-lg-8 mx-auto my-5">	
 
                 @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        {{ $error }} <br/>
                        @endforeach
                    </div>
                    @endif 
     
                    <form action="{{url('berkas/proses')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
     
                        <div class="form-group">
                            <b>File Gambar</b><br/>
                            <input type="file" name="file">
                        </div>
     
                        <div class="form-group">
                            <b>Keterangan</b>
                            <textarea class="form-control" name="keterangan"></textarea>
                        </div>
     
                        <input type="submit" value="Upload" class="btn btn-primary">
                    </form>
                    
                    <h4 class="my-5">Data</h4>
     
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="1%">File</th>
                                <th>Keterangan</th>
                                <th width="1%">OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($gambar as $item)
                           <tr>
                            <td><img width="150px" src="{{ url('/data_file/'.$item->file) }}"></td>
                            <td>{{$item->keterangan}}</td>
                            <td>
                                <form action="{{url('/berkas/delete/'.$item->id)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                    @method('delete')
                                    @csrf
                                     <button class="btn btn-danger btn-sm">
                                         <div class="i fa fa-delete">Hapus</div>

                                     </button>


                                 </form>
                                
                            </td>
                        </tr>
                           @endforeach
                          
                           
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
</div>
@endsection

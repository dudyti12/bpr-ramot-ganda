@extends('master')
@section('title','BPR GAMOT')
    
@section('breadcrumbs')
    
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Tambah Data Pemohon
            </div>
            <div class="pull-center">
                <a href="{{url('datapemohon')}}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i>kembali
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="card-body ">
                <div class="row">
                    <div class="class col-md-4 offset-md-4">
                        <form action="{{url('datapemohon/store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Nama Pemohon</label>
                                <input type="text" name="nama_pemohon" class="form-control" autofocus required>
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label>Umur</label>
                                <input type="text" name="umur" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label>Alamat Tinggal(*Sesuai KTP)</label>
                                <input type="text" name="alamat_tinggal" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label>No KTP</label>
                                <input type="text" name="no_ktp" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label>No NPWP</label>
                                <input type="text" name="no_npwp" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label>Nama Ibu</label>
                                <input type="text" name="nama_ibu" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label>Pendidikan</label>
                                <input type="text" name="pendidikan" class="form-control"  required>
                            </div>
                            <button type="submit" class="btn btn-success">save</button>
                            </div>
                            
                        </form>

                    </div>


                </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection

@extends('master')
@section('title','BPR GAMOT')
    
@section('breadcrumbs')
    
@endsection

@section('content')
<div class="content mt-3">
    @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div> 
    @endif
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Data Pemohon
            </div>
            <div class="pull-center">
                <a href="{{url('datapemohon/create')}}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i>Tambah
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pemohon</th>
                                <th>Tempat Lahir</th>
                                <th>Umur</th>
                                <th>Alamat(Sesuai KTP)</th>
                                <th>No.KTP</th>
                                <th>No.NPWP</th>
                                <th>Nama Ibu Kandung</th>
                                <th>Pendidikan</th>
                                <th>Aksi</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_pemohon as $item)
                            <tr class="odd grade">
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->nama_pemohon}}</td>
                                <td>{{$item->tempat_lahir}}</td>
                                <td class="center">{{$item->umur}}</td>
                                <td class="center">{{$item->alamat_tinggal}}</td>
                                <td class="center">{{$item->no_ktp}}</td>
                                <td class="center">{{$item->no_npwp}}</td>
                                <td class="center">{{$item->nama_ibu}}</td>
                                <td class="center">{{$item->pendidikan}}</td>
                                <td class="center">
                                    <a href="{{url('/datapemohon/edit/'.$item->id_pemohon)}}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form action="{{url('/datapemohon/delete/'.$item->id_pemohon)}}" method="POST" onsubmit="return confirm('Yakin Hapus data?')" class="d-inline">
                                       @method('delete')
                                       @csrf
                                        <button class="btn btn-danger btn-sm">
                                            <div class="i fa fa-delete">Hapus</div>

                                        </button>


                                    </form>
                                </td>

                            </tr>    
                            @endforeach
                            
                           
                        </tbody>
                    </table>
                </div>
               
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
</div>
@endsection

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//------------------------Routing DataPemohon--------------------------
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/master', 'DatapemohonController@index');
Route::get('/datapemohon', 'DatapemohonController@show');
Route::get('/datapemohon/create', 'DatapemohonController@create');
Route::post('/datapemohon/store', 'DatapemohonController@store');
Route::get('/datapemohon/edit/{id}', 'DatapemohonController@edit');
Route::patch('/datapemohon/{id}','DatapemohonController@update');
Route::delete('/datapemohon/delete/{id}','DatapemohonController@destroy');
//-------------------------Routing Berkas-----------------------------------
Route::get('/berkas','UploadController@show');
Route::post('/berkas/proses', 'UploadController@create');
Route::delete('/berkas/delete/{id}', 'UploadController@destroy');
//--------------------------logout-----------------------------------------
Route::get('/logout','HomeController@logout');